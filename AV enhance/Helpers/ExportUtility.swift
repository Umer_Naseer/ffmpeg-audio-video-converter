//
//  ExportUtility.swift
//  AV enhance
//
//  Created by Apple on 19/08/2019.
//  Copyright © 2019 shujahasan. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

class ExportUtility {
    
    static var sharedInstance = ExportUtility()
    let realm = try! Realm()
    
    // MARK: Audio Export
    
    func getDocumentsDirectoryWithAudioFolder() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("Audio_exported")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        return dataPath
        
    }
    
    func getDocumentsDirectoryWithVideoFolder() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("Video_exported")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        return dataPath
        
    }
    
    func getDestinationFilePathWithSoundLevel(name: String, soundLevel: String, fileExtension: String) -> URL {
        
        let fileNameWithExt = "\(name)\(self.getCurrentDateInString())[\(soundLevel)].\(fileExtension)"
        let destinationUrl = self.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
        return destinationUrl
        
    }
    
    func getDestinationFilePath(sourceFileUrl: URL, fileName: String, fileExtension: String, volumeBoost: String) -> URL {
        
        var volumeBoostFixed = volumeBoost.replacingOccurrences(of: " ", with: "")
        volumeBoostFixed = volumeBoostFixed.replacingOccurrences(of: ".0", with: "")
        volumeBoostFixed = volumeBoostFixed.replacingOccurrences(of: "%", with: "Percent")
        
        let fileNameWithExt = "\(fileName)-\(volumeBoostFixed).\(fileExtension)"
        let destinationUrl = self.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
        return destinationUrl
        
    }
    
    func getDestinationVideoFilePath(sourceFileUrl: URL, fileName: String, fileExtension: String, volumeBoost: String) -> URL {
        
        var volumeBoostFixed = volumeBoost.replacingOccurrences(of: " ", with: "")
        volumeBoostFixed = volumeBoostFixed.replacingOccurrences(of: ".0", with: "")
        volumeBoostFixed = volumeBoostFixed.replacingOccurrences(of: "%", with: "Percent")
        var fileExtensionNew = fileExtension
        if fileExtension.contains("mkv"){
            fileExtensionNew = "mkv"
        }
        
        let fileNameWithExt = "\(fileName)-\(self.getCurrentDateInString())-\(volumeBoostFixed).\(fileExtensionNew)"
        let destinationUrl = self.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
        return destinationUrl
        
    }
    
    func getTransformFilePath() -> URL {
        
        let destinationUrl = self.getDocumentsDirectoryWithVideoFolder().appendingPathComponent("transforms.trf")
        return destinationUrl
        
    }
    
    func getStabilizedVideoPath(fileName: String, pathExtension: String) -> URL {
        
        let fileNameWithExt = "\(fileName)_stabilised.\(pathExtension)"
        let destinationUrl = self.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
        return destinationUrl
        
    }
    
    func copyFileToDocumentsDirectory(sourceFileUrl:  URL, fileName: String) -> URL {
        
        let fileNameWithExt = "\(fileName).\(sourceFileUrl.lastPathComponent)"
        let destinationUrl = self.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
        let copied = self.secureCopyItem(sourceUrl: sourceFileUrl, to: destinationUrl)
        
        if(copied) {
            return destinationUrl
        }
        else {
            return sourceFileUrl
        }
        
    }
    
    func secureCopyItem(sourceUrl: URL, to destinationUrl: URL) -> Bool {
        do {
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                try FileManager.default.removeItem(at: destinationUrl)
            }
            try FileManager.default.copyItem(at: sourceUrl, to: destinationUrl)
        } catch (let error) {
            print("Cannot copy item at \(sourceUrl) to \(destinationUrl): \(error)")
            return false
        }
        return true
    }
    
    func deleteItemAt(sourceUrl: URL) {
        
        do {
            if FileManager.default.fileExists(atPath: sourceUrl.path) {
                try FileManager.default.removeItem(at: sourceUrl)
            }
            
        }
        catch (let error) {
            print("Cannot delete item at \(sourceUrl): \(error)")
        }
        
    }
    
    func getAudioFormatConvertFFMPEGCommand(extention: String, volumeBoost: String, sourceFilePath: String, destinationFilePath: String) -> String {
        
        var ffmpegCommand = ""
        
        if(volumeBoost.count > 0) {
            
            let volumeBoost = self.getVolumeBoostAccordingToLevel(soundLevel: volumeBoost)
            
            ffmpegCommand = "-hide_banner -y -i \(sourceFilePath) -filter:a volume=\(volumeBoost) -map_metadata 0 -movflags use_metadata_tags \(destinationFilePath)"
            
            if (destinationFilePath.range(of: "m4a") != nil) {
                ffmpegCommand = "-hide_banner -y -i \(sourceFilePath) -filter:a volume=\(volumeBoost) -map_metadata 0 -movflags use_metadata_tags -vn \(destinationFilePath)"
            }
            
        }
        else {
            
            ffmpegCommand = "-hide_banner -y -i \(sourceFilePath) -filter:a volume=1.0 -map_metadata 0 -movflags use_metadata_tags \(destinationFilePath)"
            
            if (destinationFilePath.range(of: "m4a") != nil) {
                ffmpegCommand = "-hide_banner -y -i \(sourceFilePath) -filter:a volume=1.0 -map_metadata 0 -movflags use_metadata_tags -vn \(destinationFilePath)"
            }
            
        }
        
        return ffmpegCommand
        
    }
    
    func getVideoFormatConvertFFMPEGCommand(extention: String, volumeBoost: String, sourceFilePath: String, destinationFilePath: String, musicFilePath: String, subtitleFilePath: String) -> String {
        
        var volumeBoostLevel = 1.0
        var ffmpegCommand = "-hide_banner -y -i"
        var customOptions = ""
               if extention == "x265" {
                   customOptions = "-crf 28 -preset fast"
               } else if extention == "webm" {
                   customOptions = "-crf 10 -c:v libvpx -preset slow -vb 20M -ac 2"
               }  else if extention == "mov" {
                   customOptions = "-format hap_q -c:v h264 -preset ultrafast"
               } else if extention == "mp4" {
                   customOptions = "-c:v h264 -preset slow -vb 20M"
               } else if extention == "mkv (H.264)" {
                   customOptions = "-c:v h264_videotoolbox -preset slow -vb 20M"
               } else if extention == "mkv (H.265, slow)" {
                   customOptions = "-c:v libx265 -preset slow -vb 20M"
               } else if extention == "m2ts" {
                   customOptions = "-c:v h264 -preset slow -vb 20M"
               } else if extention == "avi" {
                   customOptions = "-c:v libxvid -preset slow -vb 20M"

               } else if extention == "rmv" {
                   customOptions = "-g 12 -r 30 -s qcif -vcodec rv20"

               }else if extention == "wmv" {
                   customOptions = "-c:v wmv2 -c:a wmav2 -ac 2 -preset slow -qscale:v 3"
               }else if extention == "gif" {
                   customOptions = ""
               }else if extention == "ogg" {
                   customOptions = "-c:v libtheora -qscale:v 3 -c:a libvorbis -qscale:a 3"
               }else if extention == "mpg" {
                   customOptions = "-c:v mpeg2video -qscale:v 3 -c:a mp2 -preset slow"
               }
        

        

        ffmpegCommand = "\(ffmpegCommand) \(sourceFilePath)"

        
        
        
        
        volumeBoostLevel = self.getVolumeBoostAccordingToLevel(soundLevel: volumeBoost)
        
        if(volumeBoostLevel != 1.0 && musicFilePath.count > 0) {
            // filter_complex will be used
            // Volume and music both will be added
            
            ffmpegCommand = "\(ffmpegCommand) -i \(musicFilePath) -filter_complex [1:0]apad,amerge=inputs=2,volume=\(volumeBoostLevel) -shortest"
            
        }
        else if(volumeBoostLevel == 1.0 && musicFilePath.count > 0) {
            // filter_complex will be used
            // music will be added
            ffmpegCommand = "\(ffmpegCommand) -i \(musicFilePath) -filter_complex [1:0]apad -shortest"

        }
        else if(volumeBoostLevel != 1.0 && musicFilePath.count == 0) {
            // filter will be used
            // Volume will be added
            volumeBoostLevel = self.getVolumeBoostAccordingToLevel(soundLevel: volumeBoost)
            ffmpegCommand = "\(ffmpegCommand) -filter:a volume=\(volumeBoostLevel)"
        }
        
        // add subtitle file path
        if(subtitleFilePath.count > 0) {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0] as NSString
            let pattt = documentsDirectory.appendingPathComponent(subtitleFilePath)
            let tt = "FontName=MyFontName"
            ffmpegCommand = "\(ffmpegCommand) -vf subtitles=\(pattt):force_style='\(tt)'"
        }
        
        if customOptions != "" {
            ffmpegCommand = "\(ffmpegCommand) \(customOptions)"
        }


        // finally add destination path
        ffmpegCommand = "\(ffmpegCommand) \(destinationFilePath)"
        
        print("ffmpegCommand: \(ffmpegCommand)")
        
        

        
        return ffmpegCommand
        
    }
    
    func getVolumeBoostAccordingToLevel(soundLevel: String) -> Double {
        
        var volumeBoost = 0.0
        
        if(soundLevel == "0.0 %(Mute)") {
            volumeBoost = 0.0
        }
        else if(soundLevel == "0.1 %") {
            volumeBoost = 0.01
        }
        else if(soundLevel == "10.0 %") {
            volumeBoost = 0.1
        }
        else if(soundLevel == "20.0 %") {
            volumeBoost = 0.2
        }
        else if(soundLevel == "30.0 %") {
            volumeBoost = 0.3
        }
        else if(soundLevel == "40.0 %") {
            volumeBoost = 0.4
        }
        else if(soundLevel == "50.0 %") {
            volumeBoost = 0.5
        }
        else if(soundLevel == "60.0 %") {
            volumeBoost = 0.6
        }
        else if(soundLevel == "70.0 %") {
            volumeBoost = 0.7
        }
        else if(soundLevel == "80.0 %") {
            volumeBoost = 0.8
        }
        else if(soundLevel == "90.0 %") {
            volumeBoost = 0.9
        }
        else if(soundLevel == "100% (No Change)") {
            volumeBoost = 1.0
        }
        else if(soundLevel == "150.0 %") {
            volumeBoost = 1.5
        }
        else if(soundLevel == "200.0 %") {
            volumeBoost = 2.0
        }
        else if(soundLevel == "300.0 %") {
            volumeBoost = 3.0
        }
        else if(soundLevel == "400.0 %") {
            volumeBoost = 4.0
        }
        else if(soundLevel == "500.0 %") {
            volumeBoost = 5.0
        }
        else if(soundLevel == "600.0 %") {
            volumeBoost = 6.0
        }
        else if(soundLevel == "700.0 %") {
            volumeBoost = 7.0
        }
        else if(soundLevel == "800.0 %") {
            volumeBoost = 8.0
        }
        else if(soundLevel == "900.0 %") {
            volumeBoost = 9.0
        }
        else if(soundLevel == "1000.0 %") {
            volumeBoost = 10.0
        }
        
        return volumeBoost
        
    }
    
    func isAudioSupportedByFFMPEG(destinationFilePath: String) -> Bool {
        
        var isSupported = false
        
        if((destinationFilePath.range(of: "m4r")) != nil) {
            isSupported = true
        }
        else if((destinationFilePath.range(of: "opus")) != nil) {
            isSupported = true
        }
        else if((destinationFilePath.range(of: "wma")) != nil) {
            isSupported = true
        }
        else if((destinationFilePath.range(of: "ogg")) != nil) {
            isSupported = true
        }
        else if((destinationFilePath.range(of: "adx")) != nil) {
            isSupported = true
        }// video formats
        else if((destinationFilePath.range(of: "flv")) != nil) {
            isSupported = true
        }
        else if((destinationFilePath.range(of: "wmv")) != nil) {
            isSupported = true
        }
        else if((destinationFilePath.range(of: "webm")) != nil) {
            isSupported = true
        }
        
        return isSupported
        
    }
    
    // MARK: Audio Export End
    
    // MARK: Database Methods
    
    func getIDForNewAudioExportModel() -> Int {

        var maxID = 0
        if let id =  realm.objects(AudioExportModel.self).max(ofProperty: "id") as Int? {
            maxID = id
        }
        return maxID+1
        
    }
    
    func addAudioExportModelToDatabase(audioExportModel: AudioExportModel) {
        
        try! realm.write {
            realm.add(audioExportModel)
        }
        
    }
    
    func getAllAudioExportModelsFromDatabase() -> Results<AudioExportModel> {
        
        let audioExportModels = realm.objects(AudioExportModel.self)
        return audioExportModels
        
    }
    
    func getAudioExportModelForID(id: Int) -> AudioExportModel {
        
        let audioExportModel = realm.objects(AudioExportModel.self).filter("id == \(id)").first ?? AudioExportModel()
        return audioExportModel
        
    }
    
    func updateNameForAudioExportModelForID(id: Int, name: String) {
        
        let audioExportModel = realm.objects(AudioExportModel.self).filter("id == \(id)").first ?? AudioExportModel()
        try! realm.write {
            audioExportModel.name = name
            audioExportModel.fullName = name
        }
        
    }
    
    func updateNoteForAudioExportModelForID(id: Int, note: String) {
        
        let audioExportModel = realm.objects(AudioExportModel.self).filter("id == \(id)").first ?? AudioExportModel()
        try! realm.write {
            audioExportModel.note = note
        }
        
    }
    
    func deleteAudioExportModelForID(id: Int) {
        
        if let audioExportModel = realm.objects(AudioExportModel.self).filter("id == \(id)").first {
            let filePath = audioExportModel.filePath
            let fileUrl = NSURL(string: filePath) ?? NSURL()
            // delete file first
            self.deleteItemAt(sourceUrl: fileUrl as URL)
            
            try! realm.write {
                realm.delete(audioExportModel)
            }
        }
        
    }
    
    // Video Methods
    
    func getIDForNewVideoExportModel() -> Int {

        var maxID = 0
        if let id =  realm.objects(VideoExportModel.self).max(ofProperty: "id") as Int? {
            maxID = id
        }
        return maxID+1
        
    }
    
    func addVideoExportModelToDatabase(videoExportModel: VideoExportModel) {
        
        try! realm.write {
            realm.add(videoExportModel)
        }
        
    }
    
    func getAllVideoExportModelsFromDatabase() -> Results<VideoExportModel> {
        
        let videoExportModels = realm.objects(VideoExportModel.self)
        return videoExportModels
        
    }
    
    func getVideoExportModelForID(id: Int) -> VideoExportModel {
        
        let videoExportModel = realm.objects(VideoExportModel.self).filter("id == \(id)").first ?? VideoExportModel()
        return videoExportModel
        
    }
    
    func updateNameForVideoExportModelForID(id: Int, name: String) {
        
        let videoExportModel = realm.objects(VideoExportModel.self).filter("id == \(id)").first ?? VideoExportModel()
        try! realm.write {
            videoExportModel.name = name
            videoExportModel.fullName = name
        }
        
    }
    
    func updateNoteForVideoExportModelForID(id: Int, note: String) {
        
        let videoExportModel = realm.objects(VideoExportModel.self).filter("id == \(id)").first ?? VideoExportModel()
        try! realm.write {
            videoExportModel.note = note
        }
        
    }
    
    func deleteVideoExportModelForID(id: Int) {
        
        if let videoExportModel = realm.objects(VideoExportModel.self).filter("id == \(id)").first {
            let filePath = videoExportModel.filePath
            let fileUrl = NSURL(string: filePath) ?? NSURL()
            // delete file first
            self.deleteItemAt(sourceUrl: fileUrl as URL)
            
            try! realm.write {
                realm.delete(videoExportModel)
            }
        }
        
    }
    
    // MARK: Database Methods End

    func getCurrentDateInString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-hh-mm"
        return dateFormatter.string(from: date)
    }
    
}

extension FileManager {
    func copyfileToUserDocumentDirectory(forResource name: String,
                                         ofType ext: String) throws
    {
        if let bundlePath = Bundle.main.path(forResource: name, ofType: ext),
            let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                .userDomainMask,
                                true).first {
            let fileName = "\(name).\(ext)"
            let fullDestPath = URL(fileURLWithPath: destPath)
                                   .appendingPathComponent(fileName)
            let fullDestPathString = fullDestPath.path

            if !self.fileExists(atPath: fullDestPathString) {
                try self.copyItem(atPath: bundlePath, toPath: fullDestPathString)
            }
        }
    }
}
