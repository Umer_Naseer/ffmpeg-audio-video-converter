//
//  SettingsModel.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit

class SettingsModel {
    var type = [SettingsRowType]()
    var titleArray = [String]()
    var valueArray = [String]()
}

enum SettingsRowType: Int {
    case AudioVolume = 0,PreviewBeforeImporting,VideoFormat,AudioFormat,RemoveAdd,Restore,ShareOurApp,RateNReview,FeedBack,MoreApps,PrivacyPolicy,TermNCondition,ItunesFileSharing
}
